import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

  institution_name: string = "";
  product_name: any;
  qty: any;
  leadtime: any;
  total_all_final: any;
  letterObj: any;

  // letterObj = {
  //   to: 'Irfan',
  //   from: 'Azizi',
  //   text: 'Thanks'
  // }

  pdfObj = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private file: File,
    private fileOpener: FileOpener,
    private plt: Platform) {

    this.getCurrentData(navParams.get('product_name'), navParams.get('qty'), navParams.get('leadtime'), navParams.get('total_all_final'));

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderPage');
  }

  getCurrentData(product_name, qty, leadtime, total_all_final) {
    this.product_name = product_name;
    this.qty = qty;
    this.leadtime = leadtime;
    this.total_all_final = total_all_final;
    console.log('product_name: ', this.product_name);
    console.log('qty: ', this.qty);
    console.log('leadtime name: ', this.leadtime);
    console.log('total per pcs: ', this.total_all_final);
  }

  getDataOrder() {

    this.letterObj = {
      institution_name: this.institution_name,
      product_name: this.product_name,
      qty: this.qty,
      netto: this.total_all_final
    }
    console.log('letterObj : ', this.letterObj);
    this.createPdf();

  }

  createPdf() {
    var docDefinition = {
      content: [
        { text: this.letterObj.institution_name, style: 'header' },
        { text: new Date().toTimeString(), alignment: 'right' },

        { text: 'From', style: 'subheader' },
        { text: this.letterObj.from },

        { text: 'To', style: 'subheader' },
        this.letterObj.to,

        { text: this.letterObj.text, style: 'story', margin: [0, 20, 0, 20] },

        // {
        //   ul: [
        //     'Bacon',
        //     'Rips',
        //     'BBQ',
        //   ]
        // }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'left',
          width: '50%',
          margin: [10, 0, 0, 0]
        }
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);
  }

  downloadPdf() {
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });

        // Save the PDF to the data Directory of our App
        this.file.writeFile('file:///data/user/0/io.oem.vci/files/', 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open('file:///data/user/0/io.oem.vci/files/' + 'myletter.pdf', 'application/pdf');
        })
      });
    }

    // this.createPdf();
    // if (this.plt.is('cordova')) {
    //   try {
    //     this.pdfObj.getBuffer(async (buffer) => {
    //       var utf8 = new Uint8Array(buffer);
    //       var binaryArray = utf8.buffer;
    //       var blob = new Blob([binaryArray], { type: 'application/pdf' });

    //       const fileEntry = await this.file.writeFile('file:///data/user/0/io.oem.vci/files/', 'myletter.pdf', blob, { replace: true });
    //       this.fileOpener.open('file:///data/user/0/io.oem.vci/files/' + 'myletter.pdf', 'application/pdf');
    //     });
    //   }
    //   catch (err) {
    //     console.log(err);
    //   }

    // }
    else {
      // On a browser simply use download!
      this.pdfObj.download('po.pdf');
      console.log('download in browser');
    }
  }

}
