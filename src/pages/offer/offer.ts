import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, Slides, Content } from 'ionic-angular';
import { PostProvider } from '../../providers/post-provider';

import { ImageViewerController } from 'ionic-img-viewer';
import { OrderPage } from '../order/order';


@IonicPage()
@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html',
})
export class OfferPage {

  @ViewChild('sliders') sliders: Slides;
  @ViewChild(Content) content: Content;

  product_id: any;
  product_name: any;
  min_order: any;
  leadtime_id: any;
  leadtime_day: any;
  leadtime_id_select: any;
  list_leadtime: any;
  loader: any;
  total1: any;
  total2: any;
  percentage: any;
  rup_percentage_temp: any;
  rup_percentage: any;
  rup_percentage1: any;
  rup_percentage2: any;
  total_all: any;
  total_all_final: any;
  total_all1: any;
  label_name: any;
  image_product: any;
  qty: any;
  repeater: Number = 0;
  slider: any;
  type_label: any;
  leadtime_percent: any;
  temp_total: any;
  range_id: any;
  percentage1: any;
  percentage2: any;
  percentage_temp: any;
  flag_min_order: any;
  leadtime_name: any;

  public tap: number = 0;

  private _imageViewerCtrl: ImageViewerController;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public postPvdr: PostProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public imageViewerCtrl: ImageViewerController,
    public toastCtrl: ToastController) {

    this.getCurrentData(navParams.get('product_id'), navParams.get('product_name'), navParams.get('min_order'));
    this.total1 = 0;
    this.total2 = 0;
    this.total_all = 0;
    this.total_all_final = 0;
    this.label_name = '-';

    this._imageViewerCtrl = imageViewerCtrl;

    this.type_label = '-';

  }

  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();

    // setTimeout(() => imageViewer.dismiss(), 1000);
    // imageViewer.onDidDismiss(() => alert('Viewer dismissed'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferPage');
    this.getLeadtimeId();
    this.getImages();
    if (this.repeater == 0) {
      this.loadSetup()
    }
  }

  async presentLoading() {
    this.loader = await this.loadingCtrl.create({
      content: "",
      duration: 2000
    });
    return await this.loader.present();
  }

  getCurrentData(product_id, product_name, min_order) {
    this.product_id = product_id;
    this.product_name = product_name;
    this.min_order = min_order;
    console.log('product_id: ', this.product_id);
    console.log('product_name: ', this.product_name);
    console.log('min_order: ', this.min_order);
  }

  selectDesign(type_label) {
    this.type_label = type_label
    console.log('design : ', this.type_label);
    console.log('index : ', this.sliders.getActiveIndex());

  }

  tapEvent(e) {
    this.tap++;
    console.log('tap : ', e);
    if (this.tap == 2) {
      this.qty = this.min_order;
      this.tap = 0;
    }
  }

  getImages() {

    let body = {
      product_id: this.product_id,
      aksi: 'get_image_product'
    };

    // this.presentLoading();
    this.postPvdr.postData(body, 'ImageProduct').subscribe((data) => {

      var alertpesan = data.msg;
      if (data.success) {

        // this.loader.dismiss();

        // alert(alertpesan);

        console.log(this.sliders.getActiveIndex());


        this.image_product = [];
        for (let i = 0; i < data.result.length; i++) {

          this.image_product.push(
            {
              'product_id': data.result[i].product_id,
              'type_label': data.result[i].type_label,
              'source_image': data.result[i].source_image
            }
          )

          // this.slider = [
          //   {
          //     image: data.result[0].source_image,
          //   },
          //   {
          //     image: data.result[1].source_image,
          //   },
          //   {
          //     image: data.result[2].source_image,
          //   }
          // ];

        }

      } else {

        // this.loader.dismiss();
        const toast = this.toastCtrl.create({
          message: alertpesan,
          duration: 2000,
          position: 'top'
        });
        toast.present();

      }
    }, error => {

      // this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({

          title: 'Pehatian',
          subTitle: 'Internet tidak tersambung, cek kembali signal atau kuota internet Anda.',
          buttons: [
            {
              text: 'Reload',
              handler: () => {
                this.getImages();
                console.log('Reload clicked');
              }
            },
            {
              text: 'OK',
              handler: () => {
                console.log('Dismiss');
              }
            }
          ]

        });
      alert.present();
    });


  }

  selectLeadTimes($event) {
    this.leadtime_id_select = $event;
    console.log("Leadtime ID:", this.leadtime_id_select);
    // this.getLeadtimeId();
    if (this.repeater == 1) {
      this.getCalculateButton();
    }
  }

  selectLeadtime(id, name) {
    this.content.scrollToBottom();
    this.leadtime_id_select = id;
    this.leadtime_name = name;
    console.log("Leadtime ID:", this.leadtime_id_select);
    console.log("Leadtime ID:", this.leadtime_name);
    if (this.repeater == 1) {
      this.getCalculateButton();
    }
  }

  getLeadtimeId() {
    let body = {
      aksi: 'get_leadtime'
    };

    // this.presentLoading();
    this.postPvdr.postData(body, 'LeadTime').subscribe((data) => {

      var alertpesan = data.msg;
      if (data.success) {

        // this.loader.dismiss();
        this.list_leadtime = [];
        for (let i = 0; i < data.result.length; i++) {
          this.list_leadtime.push(
            {
              'leadtime_id': data.result[i].leadtime_id,
              'leadtime_name': data.result[i].leadtime_name,
              'leadtime_percent': data.result[i].leadtime_percent
            }
          )
        }

      } else {

        // this.loader.dismiss();
        const toast = this.toastCtrl.create({
          message: alertpesan,
          duration: 2000,
          position: 'top'
        });
        toast.present();

      }
    }, error => {

      // this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({

          title: 'Pehatian',
          subTitle: 'Internet tidak tersambung, cek kembali signal atau kuota internet Anda.',
          buttons: [
            {
              text: 'Reload',
              handler: () => {
                console.log('Reload clicked');
                this.getLeadtimeId();
              }
            },
            {
              text: 'OK',
              handler: () => {
                console.log('Dismiss');
              }
            }
          ]

        });
      alert.present();
    });
  }

  loadSetup() {

    let body = {
      aksi: 'get_setup'
    };

    // this.presentLoading();
    this.postPvdr.postData(body, 'Setup').subscribe((data) => {

      var alertpesan = data.msg;
      if (data.success) {

        // this.loader.dismiss();

        this.percentage = data.result[0].percentage;
        this.flag_min_order = data.result[0].flag_min_order;

        this.rup_percentage = this.percentage / 100;

        console.log('persen :', this.percentage);
        console.log('hasil persen :', this.rup_percentage);
        console.log('Flag min order :', this.flag_min_order);

        if (this.repeater == 1) {
          this.calculate();
        }


      } else {

        // this.loader.dismiss();
        const toast = this.toastCtrl.create({
          message: alertpesan,
          duration: 2000,
          position: 'top'
        });
        toast.present();

      }
    }, error => {

      // this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({

          title: 'Pehatian',
          subTitle: 'Internet tidak tersambung, cek kembali signal atau kuota internet Anda.',
          buttons: [
            {
              text: 'Reload',
              handler: () => {
                console.log('Reload clicked');
                this.loadSetup();
              }
            },
            {
              text: 'OK',
              handler: () => {
                console.log('Dismiss');
              }
            }
          ]

        });
      alert.present();
    });

  }

  getCalculateButton() {

    this.repeater = 1;


    // if (this.type_label == '' || this.type_label == '-') {
    //   const toast = this.toastCtrl.create({
    //     message: 'Pilih tipe label terlebih dahulu.',
    //     duration: 2000,
    //     position: 'top'
    //   });
    //   toast.present();
    // } else
    if (this.qty == 0 || this.qty == undefined) {
      console.log('qty 0 atau undifined');
      const toast = this.toastCtrl.create({
        message: 'Qty tidak boleh kosong.',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    } else if (this.leadtime_id_select == undefined) {
      console.log('leadtime 0 atau undifined');
      const toast = this.toastCtrl.create({
        message: 'Leadtime tidak boleh kosong.',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    } else {

      console.log('gada apa2');
      // this.loadSetup();

      if (this.flag_min_order == 'Y') {

        console.log('Minimum order Y');
        if (parseInt(this.qty) < parseInt(this.min_order)) {
          console.log('qty kurang dari min order');
          const toast = this.toastCtrl.create({
            message: 'Pesanan kurang dari minimum order.',
            duration: 2000,
            position: 'middle'
          });
          toast.present();
          this.total_all_final = 0;
        } else {
          console.log('Minimum order T, boleh minimum order');
          this.loadSetup();
        }

      } else {
        console.log('Minimum order T, boleh minimum order');
        this.loadSetup();
      }


    }
  }

  calculate() {
    // document.getElementById('total1').style.display = '';
    // document.getElementById('total2').style.display = '';
    // document.getElementById('label_name').style.display = '';
    document.getElementById('order').style.display = '';

    this.content.scrollToBottom(100);

    console.log('Product ID: ', this.product_id);
    console.log('Quantity: ', this.qty);
    console.log('Leadtime: ', this.leadtime_id_select);

    // this.repeater = 1;

    let body = {
      product_id: this.product_id,
      qty: this.qty,
      leadtime_id: this.leadtime_id_select,
      aksi: 'get_calculating'
    };

    this.presentLoading();
    this.postPvdr.postData(body, 'Calculating').subscribe((data) => {

      var alertpesan = data.msg;
      if (data.success) {

        this.loader.dismiss();
        // for (let i = 0; i < data.result.length; i++) {
        this.total1 = data.result[0].het_price;
        this.range_id = data.result[0].range_id;
        this.leadtime_percent = data.result[0].leadtime_percent;
        // }

        if (this.range_id == 6) {

          console.log('range id 6 : ', this.range_id);
          console.log('leadtime_id : ', this.leadtime_id_select);


          if (this.leadtime_id_select == 1) {

            console.log('leadtime id 1 : ', this.leadtime_id_select);

            // satu bulan

            this.percentage_temp = this.rup_percentage;
            this.rup_percentage_temp = 1 - this.percentage_temp;
            this.total_all = Math.round(parseFloat(this.total1) / parseFloat(this.rup_percentage_temp));
            this.total_all_final = Math.ceil(this.total_all / 100) * 100;



          } else if (this.leadtime_id_select == 2) {

            console.log('leadtime id 2 : ', this.leadtime_id_select);


            // kurang dari satu bulan

            this.percentage_temp = this.rup_percentage;
            this.rup_percentage_temp = 1 - this.percentage_temp; //10%
            this.total_all1 = Math.round(parseFloat(this.total1) / parseFloat(this.rup_percentage_temp));
            this.total_all = Math.round(parseFloat(this.total_all1) / parseFloat(this.rup_percentage_temp));
            this.total_all_final = Math.ceil(this.total_all / 100) * 100;



          }

        } else {

          console.log('range id 3 : ', this.range_id);

          this.percentage_temp = this.leadtime_percent / 100;
          this.rup_percentage_temp = 1 - this.percentage_temp;
          this.total_all = Math.round(parseFloat(this.total1) / parseFloat(this.rup_percentage_temp));
          this.total_all_final = Math.ceil(this.total_all / 100) * 100;



        }




      } else {

        this.loader.dismiss();
        const toast = this.toastCtrl.create({
          message: alertpesan,
          duration: 2000,
          position: 'top'
        });
        toast.present();

        this.total1 = 0;
        this.total2 = 0;
        this.total_all = 0;

      }

    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({

          title: 'Pehatian',
          subTitle: 'Internet tidak tersambung, cek kembali signal atau kuota internet Anda.',
          buttons: [
            {
              text: 'Reload',
              handler: () => {
                console.log('Reload clicked');
                this.loadSetup();
              }
            },
            {
              text: 'OK',
              handler: () => {
                console.log('Dismiss');
              }
            }
          ]

        });
      alert.present();
    });

  }

  orderProduct(product_name, qty, leadtime, total_all_final) {
    console.log('nama produk : ', this.product_name);
    console.log('qty : ', this.qty);
    console.log('leadtime name : ', this.leadtime_name);
    console.log('total : ', this.total_all_final);
    this.navCtrl.push(OrderPage, { product_name: this.product_name, qty: this.qty, leadtime: this.leadtime_name, total_all_final: this.total_all_final });
  }


}
