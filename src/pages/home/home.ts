import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/Storage';
import { PostProvider } from '../../providers/post-provider';
import { CataloguePage } from '../catalogue/catalogue';
import { SetupPage } from '../setup/setup';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loader: any;
  corporate: any;
  production_leadtime: any;
  percentage: any;
  name_app: any;

  constructor(
    public navCtrl: NavController,
    private storage: Storage,
    public postPvdr: PostProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController) {

    this.loadSetup();
    // this.loadDataSetup();

  }

  async presentLoading() {
    this.loader = await this.loadingCtrl.create({
      content: "",
      duration: 2000
    });
    return await this.loader.present();
  }

  loadSetup() {

    // this.storage.get('setup').then((res) => {
    // if (res == null) {

    let body = {
      aksi: 'get_setup'
    };

    this.presentLoading();
    this.postPvdr.postData(body, 'Setup').subscribe((data) => {

      var alertpesan = data.msg;
      if (data.success) {

        this.storage.set('setup', data.result);

        this.loader.dismiss();

        this.corporate = data.result[0].corporate;
        this.production_leadtime = data.result[0].production_leadtime;
        this.percentage = data.result[0].percentage;
        this.name_app = data.result[0].name_app;



      } else {

        this.loader.dismiss();
        const toast = this.toastCtrl.create({
          message: alertpesan,
          duration: 2000,
          position: 'top'
        });
        toast.present();

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({

          title: 'Pehatian',
          subTitle: 'Internet tidak tersambung, cek kembali signal atau kuota internet Anda.',
          buttons: [
            {
              text: 'Reload',
              handler: () => {
                console.log('Reload clicked');
                this.loadSetup();
              }
            },
            {
              text: 'OK',
              handler: () => {
                console.log('Dismiss');
              }
            }
          ]

        });
      alert.present();
    });

    // }
    // else {

    //   this.navCtrl.push(PresensiPage);

    // }
    // });
  }

  loadDataSetup() {
    this.storage.get('setup').then((res) => {
      this.corporate = res[0].corporate;
      this.production_leadtime = res[0].production_leadtime;
      this.percentage = res[0].percentage;
    });
  }

  getCatalogue() {
    this.navCtrl.push(CataloguePage);
  }

  getSetupPage() {
    this.navCtrl.push(SetupPage);
  }

}
