import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { PostProvider } from '../../providers/post-provider';

import { OfferPage } from '../offer/offer';

@IonicPage()
@Component({
  selector: 'page-catalogue',
  templateUrl: 'catalogue.html',
})
export class CataloguePage {

  loader: any;
  products: any;
  product_id: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public postPvdr: PostProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController) {

    this.loadMasterProduct();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CataloguePage');
  }

  async presentLoading() {
    this.loader = await this.loadingCtrl.create({
      content: "",
      duration: 2000
    });
    return await this.loader.present();
  }

  loadMasterProduct() {
    let body = {

      aksi: 'get_product'

    };

    this.presentLoading();
    this.postPvdr.postData(body, 'ProductMaster').subscribe((data) => {

      var alertpesan = data.msg;
      if (data.success) {

        this.loader.dismiss();
        this.products = [];
        for (let i = 0; i < data.result.length; i++) {
          this.products.push(
            {
              'product_id': data.result[i].product_id,
              'product_code': data.result[i].product_code,
              'product_name': data.result[i].product_name,
              'product_description': data.result[i].product_description,
              'min_order': data.result[i].min_order,
              'source_image': data.result[i].source_image
            }
          )
        }

      } else {

        this.loader.dismiss();
        const toast = this.toastCtrl.create({
          message: alertpesan,
          duration: 2000,
          position: 'top'
        });
        toast.present();

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({

          title: 'Pehatian',
          subTitle: 'Internet tidak tersambung, cek kembali signal atau kuota internet Anda.',
          buttons: [
            {
              text: 'Reload',
              handler: () => {
                console.log('Reload clicked');
                this.loadMasterProduct();
              }
            },
            {
              text: 'OK',
              handler: () => {
                console.log('Dismiss');
              }
            }
          ]

        });
      alert.present();
    });
  }

  selectProduct(product_id, product_name, min_order) {
    console.log('ID Produk: ', product_id);
    console.log('Nama Produk: ', product_name);
    console.log('Minimum Order: ', min_order);
    this.navCtrl.push(OfferPage, { product_id: product_id, product_name: product_name, min_order: min_order });
  }


}
