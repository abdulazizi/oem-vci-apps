import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { PostProvider } from '../../providers/post-provider';
import { Storage } from '@ionic/Storage';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: string = "";
  password: string = "";
  loader: any;

  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public postPvdr: PostProvider,
    private storage: Storage,
    public alertCtrl: AlertController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async presentLoading() {
    this.loader = await this.loadingCtrl.create({
      content: "",
      duration: 2000
    });
    return await this.loader.present();
  }

  loginSystem() {

    if (this.username == "" && this.password == "") {
      const toast = this.toastCtrl.create({
        message: 'Pengen langsung masuk aja, input dulu dong yang bener :p',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.username == "" || this.password == "") {
      const toast = this.toastCtrl.create({
        message: 'Plis atuh jangan di kosongin inputannya kalo mau login :(',
        duration: 3000,
        position: 'top'
      });
      toast.present();

    } else {
      let body = {
        username: this.username,
        password: this.password,
        aksi: 'login_sistem_hrd'
      };
      this.presentLoading();
      this.postPvdr.postData(body, 'LoginSystem').subscribe((data) => {
        var alertpesan = data.msg;
        if (data.success) {
          this.storage.set('user_system', data.result);
          this.loginApplication();
          this.loader.dismiss();
        } else {
          this.loader.dismiss();
          const toast = this.toastCtrl.create({
            message: alertpesan,
            duration: 2000,
            position: 'top'
          });
          toast.present();
        }
      }, error => {
        this.loader.dismiss();
        const alert = this.alertCtrl.create
          ({
            title: 'Pehatian',
            subTitle: 'Tidak tersabung dengan Internet, cek kembali signal atau kuota internet anda',
            buttons: ['OK']
          });

        alert.present();

      });
    }

  }

  loginApplication() {

    let body = {
      username: this.username,
      aksi: 'login_application'
    };
    this.presentLoading();
    this.postPvdr.postData(body, 'LoginApplication').subscribe((data) => {
      var alertpesan = data.msg;
      if (data.success) {
        this.storage.set('user_app', data.result);
        this.navCtrl.setRoot(HomePage);
        this.loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'Login Sukses',
          duration: 1000,
          position: 'top'
        });
        toast.present();
      } else {
        this.loader.dismiss();
        const toast = this.toastCtrl.create({
          message: alertpesan,
          duration: 2000,
          position: 'top'
        });
        toast.present();
      }
    }, error => {
      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          title: 'Pehatian',
          subTitle: 'Tidak tersabung dengan Internet, cek kembali signal atau kuota internet anda',
          buttons: ['OK']
        });

      alert.present();

    });
  }


}
