import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/Storage';

@IonicPage()
@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html',
})
export class SetupPage {

  corporate: any;
  production_leadtime: any;
  percentage: any;
  version: any;
  name_app: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,) {

    this.loadDataSetup();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetupPage');
  }

  loadDataSetup() {
    console.log('loadsetup');
    this.storage.get('setup').then((res) => {
      this.corporate = res[0].corporate;
      this.production_leadtime = res[0].production_leadtime;
      this.percentage = res[0].percentage;
      this.version = res[0].version_app;
    });
  }

  backPage() {
    this.navCtrl.pop();
  }

}
