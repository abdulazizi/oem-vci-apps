import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { PostProvider } from '../providers/post-provider';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/Storage';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CataloguePage } from '../pages/catalogue/catalogue';
import { OfferPage } from '../pages/offer/offer';
import { SetupPage } from '../pages/setup/setup';
import { OrderPage } from '../pages/order/order';
import { LoginPage } from '../pages/login/login';
import { OrderlistPage } from '../pages/orderlist/orderlist';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SetupPage,
    CataloguePage,
    OfferPage,
    OrderPage,
    LoginPage,
    OrderlistPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicImageViewerModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SetupPage,
    CataloguePage,
    OfferPage,
    OrderPage,
    LoginPage,
    OrderlistPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PostProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    File,
    FileOpener
  ]
})
export class AppModule { }
