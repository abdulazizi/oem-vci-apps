### :point_right: This starter repo has moved to the [ionic-team/starters](https://github.com/ionic-team/starters/tree/master/ionic-angular/official/blank) repo! :point_left:


//reload ws
npm install ws@3.3.2 --save-dev --save-exact

//http
ionic cordova plugin add cordova-plugin-advanced-http
npm install --save @ionic-native/http@4

//storage
ionic cordova plugin add cordova-sqlite-storage
npm install --save @ionic-native/sqlite@4